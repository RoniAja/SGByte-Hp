import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import css from './style'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {
    render() {
      return (
        <View style={css.container}>
          <Text style={css.welcome}>
            Welcome to React Native!
          </Text>
          <Text style={css.instructions}>
            To get started, edit App.js
          </Text>
          <Text style={css.instructions}>
            {instructions}
          </Text>
        </View>
      );
    }
  }