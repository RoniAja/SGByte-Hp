/*
0 = GET
1 = POST
2 = PUT
3 = DELETE
*/

export var baseUrl = "https://alpha.sgbyte.com/api/"

export var headers = new Headers({
    "Accept": "application/json",
    "Content-Type": "application/json"
})

export default class URI {
    static getProjectInfo = set("property/8/get_project_info", 0)
    static getData = set("link", 0)
}

function set(url, method) {
    return {
        url: url,
        method: method
    }
}
