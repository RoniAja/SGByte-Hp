import { baseUrl, headers, URI } from './URI'
import { NetInfo } from 'react-native'

const result = (json) => {
    var total = 0
    if(json["total"] != null) {
        total = json["total"]
    }
    return {
        data: json["data"],
        total: total,
        message: json["message"],
        error: json["error"]
    }
}

export default function API(uri, parameters, callback) {
    fetch(baseUrl + uri.url, {
        method: method(uri.method),
        headers: headers,
        body: parameters
    })
    .then( (response) => response.json())
    .then( (json) => {
        if(json["status"] == true) {
            console.log(uri.url + " done")
            callback(result(json), null, null)
        } else {
            console.log(uri.url + " failed")
            callback(null, result(json), null)
        }
    }).catch( (error) => {
        console.log(uri.url + " fatalError")
        callback(null, null, checkInternetConnection())
    }).done()
}

function checkInternetConnection() {
    function handleFirstConnectivityChange(isConnected) { 
        NetInfo.isConnected.removeEventListener( 'connectionChange', handleFirstConnectivityChange ); 
    } 
    NetInfo.isConnected.addEventListener( 'connectionChange', handleFirstConnectivityChange );
}

function method(id) {
    switch(id) {
        case 1:
            return "POST"
        case 2:
            return "PUT"
        case 3:
            return "DELETE"
        default:
            return "GET"
    }
}
