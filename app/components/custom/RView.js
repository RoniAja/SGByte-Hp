import React, { Component, PropTypes } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';


/*
    url: https://facebook.github.io/react-native/docs/flexbox.html
    flexbox: https://css-tricks.com/snippets/css/a-guide-to-flexbox/
    props:

    style: StyleSheet,

    justify: {
        flex-start : left to right,
        center,
        flex-end : right to left,
        space-around : items are evenly distributed in the line with equal space around them,
        space-evenly : items are distributed so that the spacing between any two items (and the space to the edges) is equal,
        space-between : items are evenly distributed in the line; first item is on the start line, last item on the end line
    },
    direction: {
        row,
        row-reverse,
        column,
        column-reverse
    },
    align: { // but the test showing
        flex-start : top to bottom, (left)
        center : center,            (center)
        flex-end : bottom to top,   (right)
        stretch : full top to bottom,
        baseline : items are aligned such as their baselines align
    }

    warp: Bool  { wrap, nowrap }

    render: Function
*/
export default class RView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const {
            children,
            style,
            justify,
            direction,
            align,
            wrap,
            render,
        } = this.props
        return(
            <View
                style={[{
                    justifyContent: justify,
                    flexDirection: direction,
                    alignItems: align
                }, style]}
            >
                {children}
            </View>
        );
    }

}