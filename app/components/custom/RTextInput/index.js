import React, { Component, PropTypes } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
} from 'react-native'
import css from './style'

/*
    url: https://facebook.github.io/react-native/docs/textinput.html
    props:

    error: String,
    errorColor: String,
    errorBorderColor: String
    text: String,
    placholder: String,
    placholderColor: String,
    underlineColor: String,

    autoCapitalize: {
        'none', 'sentences', 'words', 'characters'
    },
    keyboardType: (
        'default', 'email-address', 'numeric', 'phone-pad', 'ascii-capable', 'numbers-and-punctuation', 'url', 'number-pad', 'name-phone-pad', 'decimal-pad', 'twitter', 'web-search'
    ),
    returnType: {
        'done', 'go', 'next', 'search', 'send', 'none', 'previous', 'default', 'emergency-call', 'google', 'join', 'route', 'yahoo'
    },
    clearButtonMode: {
        'never', 'while-editing', 'unless-editing', 'always'
    },
    detectorType: {
        'phoneNumber', 'link', 'address', 'calendarEvent', 'none', 'all'
    }

    hasError: Bool,
    autoGrow: Bool,
    isSecure: Bool,
    editable: Bool,
    autoFocus: Bool,
    multiline: Bool,
    fullscreenEditing: Bool,
    clearOnFocus: Bool,

    width: Number,
    numberOfLine: Number,
    maxHeight: Number,
    maxLength: Number,
    
    onChange: Function,
    onChangeText: Function,
    onEndEditing: Function,
    onFocus: Function,
    onScroll: Function,

*/

export default class RTextInput extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            borderColor: 'black',
            borderWidth: 1
        };
    }

    showingErrorLabel() {
        if(this.props.hasError) {
            return(
                <Text style={css.errorText}>{this.props.error}</Text>
            );
        } else {
            return null
        }
    }

    render() {
        const {
            error,
            errorColor,
            errorBorderColor,

            text,
            placeholder,
            placeholderColor,
            underlineColor,
        
            autoCapitalize,
            keyboardType,
            returnType,
            clearButtonMode,
            detectorType,

            autoGrow,
            isSecure,
            editable,
            autoFocus,
            multiline,
            hasError,
            fullscreenEditing,
            clearOnFocus,
        
            width,
            numberOfLine,
            maxHeight,
            maxLength,

            style,
            onFocus,
            onScroll,
            onChange,
            onChangeText,
            onEndEditing,
        } = this.props

        return(
            <View style={{width: width}}>
                <TextInput
                    style={[style, (hasError ? css.errorTextInput : css.textInput), css.spacing]}
                    defaultValue={text}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderColor}
                    underlineColorAndroid='transparent'

                    autoCorrect={false}
                    autoCapitalize={autoCapitalize}
                    keyboardType={keyboardType}
                    returnKeyType={returnType}
                    clearButtonMode={clearButtonMode}
                    dataDetectorTypes={detectorType}

                    autoGrow={autoGrow}
                    secureTextEntry={isSecure}
                    editable={editable}
                    autoFocus={autoFocus}
                    multiline={multiline}
                    disableFullscreenUI={!fullscreenEditing}
                    clearOnFocus={clearOnFocus}

                    numberOfLines={numberOfLine}
                    maxHeight={maxHeight}
                    maxLength={maxLength}

                    onFocus={onFocus}
                    onScroll={onScroll}
                    onChange={onChange}
                    onChangeText={onChangeText}
                    onEndEditing={onEndEditing}
                />
                {this.showingErrorLabel()}
            </View>
        );
    }
}