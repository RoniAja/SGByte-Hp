import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    spacing: {
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 2,
        paddingBottom: 2,
    },
    textInput: {
        borderColor: 'gray',
        borderWidth: 1
    },
    errorTextInput: {
        borderColor: this.props.errorBorderColor,
        borderWidth: 3
    },
    errorText: {
        color: this.props.errorColor,
        paddingLeft: 12,
        paddingRight: 12
    }
});