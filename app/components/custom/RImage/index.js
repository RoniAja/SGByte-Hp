import React, { Component, PropTypes } from 'react';
import {
    View,
    Image,
    StyleSheet,
    ActivityIndicator
} from 'react-native'
import css from './style.js'


/*
    url: https://facebook.github.io/react-native/docs/image.html#onerror
    props:
    style: StyleSheet,

    mode: {
        'cover', 'contain', 'stretch', 'repeat', 'center'
    },
    method: {
        'auto', 'resize', 'scale'
    },
    
    source: String, (accept: URL and image from assets),
    origin: String, (accept: URL and image from assets),
    indicatorColor: String

    onLoadStart: Function,
    onLoad: Function,
    onProgress: Function,
    onClick: Function,
*/

export default class RImage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            source: props.source,
            animating: true,
            blurValue: 10
        }

        this.onLoadEnd = this.onLoadEnd.bind(this);
        this.onError = this.onError.bind(this);
    }

    onLoadEnd() {
        this.setState({ blurValue: 0 });
        this.setState({ animating: false });
    }

    onError() {
        this.setState({ blurValue: 0 });
        this.setState({ animating: false });
        this.setState({ source: this.props.origin });
    }

    render() {
        const {
            style,
            mode,
            method,
            onLoadStart,
            onLoad,
            onProgress,
            onClick
        } = this.props

        return (
            <View stlye={css.main}>
                <ActivityIndicator
                    style={css.indicator}
                    size='large'
                    color={this.indicatorColor()}
                    animating={this.state.animating}
                />
                <Image
                    style={style}
                    blurRadius={this.state.blurValue}
                    resizeMode={mode}
                    source={this.state.source}
                    onLoadStart={onLoadStart}
                    onLoadEnd={this.onLoadEnd}
                    onProgress={onProgress}
                    onClick={onClick}
                    onLoad={onLoad}
                    onError={this.onError}
                />
            </View>
        );

    }

    indicatorColor() {
        if (this.props.indicatorColor == undefined) {
            return 'gray'
        }
        return this.props.indicatorColor
    }

}